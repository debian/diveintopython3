diveintopython3 (20110517.77958af+dfsg-3) unstable; urgency=medium

  * d/control:
    - Bump Standards-Version to 3.9.8 (no changes needed)

 -- Christian Kastner <ckk@debian.org>  Sun, 17 Apr 2016 16:42:48 +0200

diveintopython3 (20110517.77958af+dfsg-2) unstable; urgency=medium

  * d/control:
    - Bump Standards-Version to 3.9.7 (no changes needed)
    - Replace insecure URIs with secure ones
  * d/source/lintian:
    - Fix override pattern for source-is-missing
    - Add override for source-contains-prebuilt-javascript-object

 -- Christian Kastner <ckk@debian.org>  Sun, 20 Mar 2016 20:03:06 +0100

diveintopython3 (20110517.77958af+dfsg-1) unstable; urgency=medium

  * Improved version number.
  * d/control:
    - Switch Maintainer email to my @debian.org address
    - Update Vcs-Browser URL (cgit instead of gitweb)
  * d/copyright:
    - Bump copyright years
    - Drop entry for removed file html5.js
  * lintian:
    - Add source override for source-is-missing false positive
  * d/rules:
    - Remove all references to removed file html5.js
    - Switch to xz compression in get-orig-source
    - Update version processing for get-orig-source

 -- Christian Kastner <ckk@debian.org>  Thu, 01 Oct 2015 23:14:55 +0200

diveintopython3 (20110517+77958af-3) unstable; urgency=medium

  * The collab-maint repository was switch from subversion to git
  * debian/control:
    - Switch Vcs-* URLs from svn to git
    - Bump Standards-Version to 3.9.6 (no changes needed)

 -- Christian Kastner <debian@kvr.at>  Mon, 06 Oct 2014 11:25:59 +0200

diveintopython3 (20110517+77958af-2) unstable; urgency=low

  * debian/control:
    - Bump Standards-Version to 3.9.5 (no changes needed)
    - Drop obsolete DM-Upload-Allowed field
    - Use canonical Vcs-* URLs
    - Point Homepage to a copy of the original site
  * debian/rules:
    - Change dead *.org links within the source to the active *.net copies
  * debian/copyright:
    - Refresh copyright year
  * debian/source/lintian-overrides:
    - Add override for debian-watch-file-is-missing (project has terminated)
  * debian/diveintopython3.lintian-overrides:
    - Add override for no-upstream-changelog
  * debian/patches (added)
    - 0001-Fix-typos
      Fix typos in the documentation. Reported by Jakub Wilk (thanks)

 -- Christian Kastner <debian@kvr.at>  Thu, 13 Mar 2014 22:19:58 +0100

diveintopython3 (20110517+77958af-1) unstable; urgency=low

  * Upstream has terminated the project, therefore this will be the last
    release. 

  [ Christian Kastner ]
  * Updated to newest upstream version (only minor fixes).
  * debian/control:
    - Bumped Standards-Version to 3.9.3 (no changes needed)
    - Bumped debhelper dependency to 9
  * debian/compat:
    - Bumped to recommended level 9
  * debian/rules:
    - get-orig-source target has been rewritten to grab a tarball from GitHub
      based on the git commit ID in the package version. Additionally, download
      links have been rewritten to point to GitHub.
    - In the process, it was discovered that JQuery (jquery.js) shipped by
      upstream was not excluded from the source package as originally intended,
      only from the binary packages. This has been fixed; JQuery is now
      removed in the get-orig-source target, and all other references to it
      have been removed.
    - Removed the permissions fix for the example scripts from
      dh_installexamples, as it had no effect (permissions were already being
      fixed at the source repackaging level).
    - Removed exclusion filter for JavaScript files from dh_compress, as it is
      no longer needed with debhelper 9.
  * debian/watch:
    - Removed (no more updates coming).
  * debian/diveintopython3.links:
    - In doc directory, added symlink from examples to html/examples.
      Closes: #674584
  * debian/copyright:
    - Corrected the full license text of the unported CC-BY-SA-3.0 license; the 
      existing text was that of the US-ported version. 
    - Updated to machine-readable copyright format version 1.0
    - Refreshed copyrights 
    - Updated Source field
    - Removed Upstream-Contact, as upstream does not wish to be contacted

  [ Jakub Wilk ]
  * Fix formating of formatted text fields in the changelog file.
  * Set DM-Upload-Allowed.

 -- Christian Kastner <debian@kvr.at>  Sat, 30 Jun 2012 18:31:33 +0200

diveintopython3 (20110218-1) unstable; urgency=low

  * Initial release (Closes: #612100)

 -- Christian Kastner <debian@kvr.at>  Fri, 11 Mar 2011 22:56:38 +0100
