#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1


PKGDIR = $(dir $(firstword $(MAKEFILE_LIST)))..
UPDATE = $(shell cd $(PKGDIR) && dpkg-parsechangelog -SVersion | \
		sed -rne 's,([^.]+).*,\1,p')
UPCOMMIT = $(shell cd $(PKGDIR) && dpkg-parsechangelog -S Version | \
		sed -rne 's,^[[:digit:]]+\.([[:xdigit:]]+)\+dfsg-.*,\1,p')
RAW_SOURCE = diveintomark-diveintopython3-$(UPCOMMIT)
ORIG_SOURCE = diveintopython3-$(UPDATE).$(UPCOMMIT)+dfsg.orig

# We want to download from GitHub using a specific commit, not a tag.
# Therefore, we cannot use uscan or githubredir.
DOWNLOAD_URL = https://github.com/diveintomark/diveintopython3/tarball
ZIP_URL = https://github.com/diveintomark/diveintopython3/zipball/master
PDF_URL = https://github.com/downloads/diveintomark/diveintopython3/dive-into-python3.pdf


%:
	dh $@ 


# Use package libjs-jquery; mangle URLs
override_dh_install:
	dh_install

	# Remove all references to file with lost source
	sed -i -r -e 's|<!--\[if IE\]><script src=j/html5\.js></script><!\[endif\]-->||' \
		debian/diveintopython3/usr/share/doc/diveintopython3/html/*.html

	# Point to local Python3 docs instead of official at docs.python.org
	sed -i -e \
		's|http://docs\.python\.org/3\..|file:///usr/share/doc/python3/html|' \
		debian/diveintopython3/usr/share/doc/diveintopython3/html/*.html

	# Rewrite download URLS to point to GitHub
	sed -i -e \
		's|d/diveintopython3-html-latest.zip|$(ZIP_URL)|' \
		debian/diveintopython3/usr/share/doc/diveintopython3/html/index.html
	sed -i -e \
		's|d/diveintopython3-pdf-latest.zip|$(PDF_URL)|' \
		debian/diveintopython3/usr/share/doc/diveintopython3/html/index.html

	# Remove link to cover image missing from ZIP file
	sed -i -e \
		's|<br><img src="i/cover.jpg"[^<]*||' \
		debian/diveintopython3/usr/share/doc/diveintopython3/html/index.html

	# Rewrite all dead diveintopython{,3}.org URLs to the active .net copy
	sed -i -r -e \
		's|(diveintopython3?)\.org|\1.net|g' \
		debian/diveintopython3/usr/share/doc/diveintopython3/html/*.html


override_dh_installexamples:
	dh_installexamples
	
	# Rewrite all dead diveintopython{,3}.org URLs to the active .net copy
	# (This time, for the examples)
	sed -i -r -e \
		's|(diveintopython3?)\.org|\1.net|g' \
		debian/diveintopython3/usr/share/doc/diveintopython3/examples/*.py


# Don't compress anything in examples, and exclude some javascript files
override_dh_compress:
	dh_compress -Xexamples


get-orig-source:
	which wget >/dev/null || { echo "Package 'wget' required"; exit 1; }
	
	wget -O $(RAW_SOURCE).tar.gz $(DOWNLOAD_URL)/$(UPCOMMIT)
	tar -xzf $(RAW_SOURCE).tar.gz
	rm  -f $(RAW_SOURCE).tar.gz
	
	# The following steps conform to the ZIP-archive creation section in
	# upstream's ./publish script, titled "building HTML distribution".
	# They cook the source to the form we are accustomed to.
	mkdir -p $(ORIG_SOURCE)/i $(ORIG_SOURCE)/j $(ORIG_SOURCE)/examples
	cd $(RAW_SOURCE) && \
		cp *.html dip3.css ../$(ORIG_SOURCE)/ && \
		cp i/*.png ../$(ORIG_SOURCE)/i/ && \
		cp j/dip3.js j/prettify.js ../$(ORIG_SOURCE)/j/ && \
		cp examples/*.jpg examples/*.json examples/*.pickle examples/*.py \
			examples/*.txt examples/*.xml ../$(ORIG_SOURCE)/examples/
	
	# Cleanup source
	find $(ORIG_SOURCE) -type f -exec chmod 644 '{}' \;

	tar --xz --owner=root --group=root --mode=a+rX \
			-cf diveintopython3_$(UPVER)+$(UPCOMMIT)+dfsg.orig.tar.xz \
			$(ORIG_SOURCE)
	
	rm -rf $(RAW_SOURCE) $(ORIG_SOURCE)
